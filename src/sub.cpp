#include "sub.hpp"

namespace tcl
{
namespace subtraction
{
Engine::Engine() : _result(0) {}
Engine::~Engine() = default;

void Engine::process(int a, int b)
{
    _result = a - b;
}

int Engine::result() const
{
    return _result;
}

} // namespace subtraction
} // namespace tcl
